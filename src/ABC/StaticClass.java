package ABC;

public class StaticClass {
	
	public static int someVar = 10;

	public static void a() {
		System.out.println("A");
	}
	
	public static void b() {
		System.out.println("B");
	}
	
	public static void c() {
		System.out.println("c");
	}
	
	public static void d()
	{
		System.out.println("D");
	}
}
