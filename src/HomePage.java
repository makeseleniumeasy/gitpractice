
public class HomePage extends BasePage{

	public HomePage changeProfilePic(String fileName)
	{
		System.out.println(fileName +" changed");
		return this;
	}
	
	public HomePage changePassword(String newPassword)
	{
		System.out.println(newPassword +" updated");
		return this;
	}
}
