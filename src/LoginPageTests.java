
public class LoginPageTests {

	public static void main(String[] args) {
		
		LoginPage loginPage = new LoginPage();
		BasePage basePage = loginPage
			.openLoginPage()
			.enterUserName()
			.enterPassword()
			.clickOnLogin();
		
		if(basePage instanceof HomePage)
		{
			HomePage homePage = (HomePage) basePage;
			homePage.changePassword("eew").changeProfilePic("a/b/c");
		}
		else
		{
			LoginPage loginPage2 = (LoginPage) basePage;
			loginPage2.enterUserName().enterPassword().clickOnLogin();
		}
			
		
		
		
		//.changePassword("Dds")
			//.changeProfilePic("a/b/c");
				
	}
}
