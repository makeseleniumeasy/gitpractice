
public class SomeClass {

	public SomeClass a()
	{
		System.out.println("A");
		return this;
	}
	
	public SomeClass b()
	{
		System.out.println("B");
		return this;
	}
	
	public SomeClass c()
	{
		System.out.println("C");
		return this;
	}
	
	
}
