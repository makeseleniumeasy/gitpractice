
public class Usage {

	public static void main(String[] args) {
		
		SomeClass someClass = new SomeClass();
		someClass.a();
		someClass.b();
		someClass.c();
		
		someClass.a().b().c();
	}
}
