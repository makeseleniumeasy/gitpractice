import java.util.HashMap;

import static ABC.StaticClass.*;

public class UsageOfStaticMethod {

	public static void main(String[] args) {
		
		a();
		b();
		c();
		
		HashMap<String, String> m = new HashMap<>();
		
//		StaticClass staticClass = new StaticClass();
//		staticClass.a();
	}
}
